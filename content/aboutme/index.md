---
title: "About me"
date: "2024-06-18"
tags:
  - aboutme
draft: false
---

![me](images/me.jpg)
---

I'm David Henderson, I’m a dedicated tech enthusiast with a deep love for all things network and automation, a Senior Solution Architect | Strategic Network & Systems Expert with 20+ Years of Cross-Functional Experience

With over two decades in network engineering, system administration, and automation, my career is defined by deep technical expertise and strategic leadership. My journey through roles in TAC Customer Service, AT&T Project Management, Systems Engineering, and as a Senior Network Engineer has equipped me with a unique perspective on both WAN and LAN infrastructures.

As a Senior Solution Architect, I excel in understanding complex IT environments, ensuring that every project I manage from inception to completion is aligned with both client expectations and industry best practices.

### Now for some Personal Stuff:

But beyond the tech, my heart belongs to my family. As a proud father of five wonderful children, including two with special needs, my life is filled with both incredible joys and unique challenges. The journey of raising children with special needs has given me profound insights into the struggles and triumphs that so many parents experience daily. It’s a journey that has taught me patience, resilience, and the true meaning of unconditional love.

This personal experience fuels my passion for advocacy. I am committed to supporting families facing similar challenges and am actively involved in volunteer work whenever I can. Whether it’s lending a hand at local organizations, or participating in community events. 

---

### Credentials

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="bedd9003-1c27-43b1-969f-71b64f74bfb0" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="1d4190ed-1c8a-4f20-8e81-b7c3a7f8ca20" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="b7e34032-68e5-435c-b78a-562daebc7beb" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="cff0146c-955d-49f1-84ce-17654e063530" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="cc79403a-b862-4ae2-ae0e-10b860fbaef3" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="fb4de3e9-54f4-4e8b-9d76-36168338d352" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="97164db6-1353-43d1-a393-02c499d69338" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>
<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="c1ca69ca-2e6f-40bb-b7e8-32ae0c24f907" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>


