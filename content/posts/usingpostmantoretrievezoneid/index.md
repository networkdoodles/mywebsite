---
title: "Using Postman to retrieve Zone ID from Cloudflare"
date: "2024-06-27"
description: "Discover the power of YAML (Yet Another Markup Language) in network engineering a crucial tool for configuring and managing complex network infrastructures. This article breaks down YAML’s essential components and demonstrates its practical applications, for modern network automation and configuration tasks."
ShowToc: true
cover:
  image: images/yaml.png
tags:
  - API
  - DNS
  - cloudflare
draft: true
---

## Using Postman to retrieve Zone ID from Cloudflare

Fork my Cloudflare postman collection:

[<img src="https://run.pstmn.io/button.svg" alt="Run In Postman" style="width: 128px; height: 32px;">](https://app.getpostman.com/run-collection/36577954-dbf6465d-6f15-4006-be9b-14aee668a51c?action=collection%2Ffork&source=rip_markdown&collection-url=entityId%3D36577954-dbf6465d-6f15-4006-be9b-14aee668a51c%26entityType%3Dcollection%26workspaceId%3D878aa013-1618-439d-8e3e-9833635ae29d)

<img src="images/pic1.png" alt="step1" width="200"/>
<img src="images/pic2.png" alt="step2" width="200"/>
<img src="images/pic3.png" alt="step3" width="200"/>
<img src="images/pic4.png" alt="step4" width="200"/>
<img src="images/pic5.png" alt="step5" width="200"/>
<img src="images/pic6.png" alt="step6" width="200"/>

> This is a note.


> This is a warning.
{.warning}

> This is dangerous.
{.danger}

- First item
- Second item
{.special-list}