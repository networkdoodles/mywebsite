---
title: "How to Use NetBox as an Inventory in Ansible Core"
author: "David Henderson"
date: "2024-07-02"
description: "How to Use NetBox as an Inventory in Ansible Core where I show you how into integrate NetBox Labs, with Ansible by Red Hat Core using a YAML configuration file. This combination allows you to dynamically manage your network inventory and automate tasks with ease.
Article includes:
Step-by-step guide on setting up NetBox as an inventory source in Ansible Core and YAML configuration examples for seamless integration."
ShowToc: true
cover:
  image: posts/netboxasinventory/images/netbox-anisble.png
tags:
  - ansible
  - netbox
  - automation
draft: false
---

# Introduction to NetBox and Ansible Core

**NetBox** is an open-source tool designed for network automation and infrastructure management, specializing in managing IP address allocations and data center infrastructure. **Ansible Core** is a widely used automation tool that allows you to automate software provisioning, configuration management, and application deployment.

Integrating NetBox with Ansible Core can centralize your network inventory, making it easier to manage and automate your network devices. This integration leverages NetBox’s API to dynamically pull inventory data into Ansible using a YAML configuration file.

## Prerequisites

### Before starting, ensure you have the following:

- NetBox: Installed and configured. You can find installation instructions [here](https://netbox.readthedocs.io/en/stable/installation/)
- Ansible Core: Installed on your control node. Install Ansible Core by following the official [Ansible installation guide](https://docs.ansible.com/ansible/latest/installation_guide/index.html)
- NetBox API Token: Generate an API token in NetBox for authentication purposes. [NetBox API documentation](https://netbox.readthedocs.io/en/stable/)

## Configure NetBox as an Inventory Source Using YAML File

#### To use NetBox as an inventory source in Ansible, follow these steps:

1. Install the netbox.netbox Ansible Collection

Install the netbox.netbox collection which provides the inventory plugin:
{{< highlight yaml >}}
 ansible-galaxy collection install netbox.netbox
{{< /highlight >}}

2. Create a YAML Configuration File

Create a YAML configuration file named netbox_inventory.yml in your Ansible directory. This file will define how Ansible pulls inventory data from NetBox.

{{< highlight yaml >}}
nano /etc/ansible/netbox_inventory.yml 
{{< /highlight >}}

{{< admonition type="note" title="Inventory file contents:" >}}
 Update variables with information from your netbox installation
 {{< /admonition >}}

{{< highlight yaml >}}
plugin: netbox.netbox.nb_inventory
api_endpoint: {{ netbox IP }}
token: {{ netbox Token. }}
validate_certs: false
config_context: false
flatten_custom_fields: true
group_by:
   # - sites
   # - device_roles
   # - platforms
   # - locations
query_filters:
    # restrict inventory to specific tenant
    # - tenants: {{ tenant name }}
device_query_filters:
    # - has_primary_ip: 'true'
    # You can also filter by platform, role, e.g.:
    # - platform: cisco-ios-xe
    # - role: router
    # - role: switch
{{< /highlight >}}

3. Update Ansible Configuration

Modify your ansible.cfg to use the YAML file as the inventory source:

Open your Ansible config file with your favorite editor. I'm using nano

{{< highlight yaml >}}
nano /etc/ansible/ansible.cfg
{{< /highlight >}}


find the inventory line under [defaults] and update it with the location of your new inventory file

{{< highlight yaml >}}
inventory=/etc/ansible/netbox_inventory.yml
{{< /highlight >}}

4. Test Your Inventory Configuration

{{< highlight yaml >}}
 ansible-inventory --list
{{< /highlight >}}

 This command should return your inventory in JSON format, showing the hosts pulled from NetBox.

5. Example Playbook Using NetBox Inventory

Create a sample Ansible playbook to ensure your inventory is working. Here’s a simple playbook that pings all devices:

{{< highlight yaml >}}
nano playbook.yml
{{< /highlight >}}

#### playbook contents:


{{< highlight yaml >}}
- name: Test NetBox Inventory
  hosts: all
  tasks:
    - name: Ping the hosts
      ansible.builtin.ping:
{{< /highlight >}}

#### Run the playbook:

{{< highlight yaml >}}
ansible-playbook playbook.yml
{{< /highlight >}}

5. Enhancing Your Configuration

{{< admonition type="tip" >}}
You can further enhance your configuration by adjusting the group_by, device_query_filters, and query_filters options to better fit your network environment. This helps in organizing your devices into relevant groups and filtering inventory based on specific criteria.
{{< /admonition >}}

#### Here’s an example of grouping by device roles and platforms:

{{< highlight yaml >}}
group_by:
  - device_roles
  - platforms
And a filter to include only devices with a specific tenant:

query_filters:
  - tenants: YourTenantName
You can also add device_query_filters to only include devices with specific details:

device_query_filters:
    - has_primary_ip: 'true'
    # You can also filter by platform, role, e.g.:
    - platform: cisco-ios-xe
    - role: router
    - role: switch
{{< /highlight >}}

6. Best Practices

{{< admonition type="danger" title="Security:" >}}
Store your API token securely, and avoid hardcoding it in the YAML file.
File Location: Place your inventory YAML file in a dedicated directory to keep your project organized.
{{< /admonition >}}

{{< admonition type="note" title="Documentation:" >}}
Comment your configuration for better maintenance and collaboration.
Testing: Regularly test your inventory configuration to ensure it reflects your network accurately.
{{< /admonition >}}

Explore the NetBox and Ansible documentation for more advanced configurations and integrations. Happy automating!

**References:**

[NetBox Documentation](https://netbox.readthedocs.io/en/stable/)

[Ansible Documentation](https://docs.ansible.com/ansible/latest/index.html)

[NetBox Ansible Collection](https://galaxy.ansible.com/ui/repo/published/netbox/netbox/)