---
title: "Managing TLS Certificates Using Ansible and Let’s Encrypt! PART 2:"
author: "David Henderson"
date: "2024-07-21"
weight: 2
description: This tutorial will guide you through using Ansible AWX/AAP to request a Let's Encrypt certificate and securely storing your Cloudflare API key. This is the Second part of a series on managing SSL certificate lifecycles using Ansible, progressing from Ansible Core to advanced automation with AWX, Ansible Automation Platform, and integration with GitOps and ServiceNow.
ShowToc: true
cover:
  image: posts/images/Letsencrypt_2.png
tags:
  - ansible
  - certificates
  - automation
  - letsencrypt
draft: false
---


# Part 2: Managing Certificates with AWX/Ansible Automation Platform and GitOps

## Introduction

Continuing from Part 1, this tutorial will guide you through managing Let's Encrypt certificates using AWX/Ansible Automation Platform. We can use the exact same playbook from part 1 with a few modifications. You will also have to make sure you have a few Prerequisites setup a head of time.

## Prerequisites

- **AWX/Ansible Automation Platform** installed and configured. In this part will be using the Ansible GUI version AWX/AAP
- **GitLab** - AWX/AAP both require that your playbooks are stored in a central git repository I will be using GitLab. You will need two repositories:
    1. The first repository will be for holding the playbook and other required files. You can copy or clone my [repository](https://gitlab.com/networkdoodles/manage-lets-encrypt-certs-with-ansible-part2) 
    2. The second will be a repository for storing your certificates.
- **Credentials** for AWX/Ansible Automation Platform and GitLab 
- **Cloudflare account** with API token
- **Basic knowledge of Git**.

### What You Will Learn

- **Configuration of AWX/Ansible Automation Platform projects**
- **Integrating AWX with GitLab**
- **Running Playbooks and Automating Certificate Requests**

# Gitlab setup

## 1. Setup Repositories
---
#### 1.1. Copy or clone my [repository](https://gitlab.com/networkdoodles/manage-lets-encrypt-certs-with-ansible-part2) for holding the playbook and other required files.

{{< admonition type="tip" title="Folder Structure" >}} 
  - request_cert.yml - main playbook
  - Collections
    - requirements.yml - AWX/AAP reads this file and installs dependant modules.
{{< /admonition >}}
Your repository should look like this
![Image1](images/image1.png)

#### 1.2 Create a new blank project in gitlab named cert-store
Create a empty file named cert_list.txt

**Your repository should look like this**
![Image2](images/image2.png)


# 1. Ansible Setup Configuring AWX/AAP

## 1.1 Setting Up AWX
---
Ensure that AWX or Ansible Automation Platform is installed and accessible. Follow the official [AWX installation guide](https://github.com/ansible/awx) if needed.

## 1.2 Creating custom credentials
---
AWX/AAP has built in templates for adding passwords and variables. but not all credentials fit into their templates. SO they give you the ability to create custom credentails.

We need to create a few Custom credentials:

  - gitlab credentiails for our cert-store repository.
  - Cloudflare API

1. **Navigate to Administration** go to the **Credential Types** section.
2. **Add a New Credential Type**: Click **Add** 
![Image3](images/image3.png)
and fill in the following details:
   - **Name**: 
   ```
   API_Key
   ```
   - **Input Configuration**:
   ```yaml
   ---
   fields:
     - id: api_key
       type: string
       label: API Key
       secret: true
   required:
     - api_key
   ```
   - **Injector configuration**:

   ```yaml
   ---
   extra_vars:
     API_key: '{{ api_key }}'
   ```
3. **Add another New Credential Type**: Click **Add** and fill in the following details:
   - **Name**: 
   ```
   Gitlab Credentials
   ```
   - **Input Configuration**:
   ```yaml
   ---
   fields:
     - id: gitlab_username
       type: string
       label: GitLab username
       secret: false
     - id: gitlab_password
       type: string
       label: GitLab password
       secret: true
     - id: gitlab_project
       type: string
       label: GitLab project
       secret: false
   required:
     - gitlab_username
     - gitlab_password
   ```
   - **Injector configuration**:

   ```yaml
   ---
   extra_vars:
     gitlab_project: '{{ gitlab_project }}'
     gitlab_password: '{{ gitlab_password }}'
     gitlab_username: '{{ gitlab_username }}'
   ```

## 1.3 Creating New Credentials using our ne custom templates
---
1. **Navigate to Resources** go to the **Credentials** section.
2. **Add a New Credential**: Click **Add** and fill in the following details:
![Image4](images/image4.png)

The first one we will create is the CloudFlare API token. use the template we created API_Key
   - **Name**: CloudFlare API Key
   - **Organization**: Your organization or default
   - **Credential Type**: API_Key
   - **Type Details**: Enter your GitLab credentials (username and password/token).
   ![Image5](images/image6.png)
Gitlab Credentials
3. **Add another custom credential**: Click **Add again**

Next we will create credentials for Gitlab so we can clone and update teh cert-store repository. CloudFlare API token. use the template we created API_Key
Fill in the following details:
   - **Name**: gitLab - cert-store
   - **Organization**: Your organization or default
   - **Credential Type**: Gitlab Credentials
   - **Type Details**: Enter your GitLab credentials for cert-store (username, password, project=cert-store).
   ![Image7](images/image7.png)

1. **Add a New Source Control Credential**:
Now we will create credentials so we can run our playbook. We will need to create a source control credential for the Gitlab project that contains the playbook request_cert.yml
and fill in the following details:
   - **Name**: gitLab Access
   - **Organization**: Your organization or default
   - **Credential Type**: Source Control
   - **Inputs**: Enter your GitLab credentials (username and password/token).
   ![Image5](images/image5.png)

## 1.4 Creating a New Project
---
### Projects are how AWX/AAP sync the playbooks from your gitlab repository to the local execution evironment.

1. **Navigate to Resources** go to the **Projects** section.
2. **Add a New Project**: Click **Add**
![Image8](images/image8.png)
fill in the following details:
   - **Name**: your_repo_name - main(branch)
   - **Organization**: Your organization
   - **SCM Type**: Git
   - **SCM URL**: ```https://gitlab.com/your-username/your-repo.git```
   - **SCM Branch/Tag/Commit**: main
   - **SCM Update Options**: Check **Clean** and **Delete** before update.
   ![Image9](images/image9.png)

## 1.5 Creating a New Job Template
---
### Templates are how AWX/AAP defines specific parameters, configurations and run playbooks

1. **Navigate to Resources** go to the **Templates** section.
2. **Add a New Job Template**: Click **Add**

   ![Image10](images/image10.png)

fill in the following details:
   - **Name**: your_repo_name - playbook name
   - **Job Type**: Run
   - **Inventory**: Demo
   - **Project**: project_created_in_last_step
   - **Playbook**: `request_cert.yml`
   - **Credentials**: Select the credentials for cert-store and cloudflare API.

## 1.6 Add a Survey to the template
---
### Once the template is create we will add a survey to collect the certificate url and email variables.

3. After you saved your template you should have been teken to the details screen. from there click **survey**
   ![Image11](images/image11.png)
4. enable the survey option and create two questions
   ![Image13](images/image13.png)
   - Question 1: Url for the cert
     - Variable name: ```fqdn```
     ![Image12](images/image12.png)
   - Question 2: Enter Email Address
     - Variable name: ```email```
     ![Image14](images/image14.png)

# 2. Running Playbooks
---

### The playbook is run from the template we just created

1. **Navigate to Resources** go to the **Templates** section.
2. Click **Rocket icon** to the right of template we just created
   ![Image15](images/image15.png)

   if sucessfull a sucess message will be displayed and your cert will be in your cert-store gitlab repository
   ![Image16](images/image16.png)

3. **Verify in GitLab**: Check your GitLab repository to ensure the certificates are updated.

   ![Image17](images/image17.png)

## Conclusion

In this part, you learned how to manage Let's Encrypt certificates using AWX/Ansible Automation Platform integrated with GitLab for GitOps. This setup allows automated certificate requests, storage, and versioning.

### Next Steps

- **Part 3**: Scheduling Auto-Renewal workflows and automating the deployment of certificates to Firewalls and F5's.
- **Part 4**: ServiceNow Integration.
- **BONUS**: Using ThousandEyes with Event-Driven Ansible to renew Certs before they expire.

Stay tuned for the next part of the series, where we will cover advanced automation techniques for certificate deployment and renewal.