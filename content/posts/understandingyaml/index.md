---
title: "Understanding YAML Files: A Guide for Network Engineers"
date: "2024-06-26"
description: "Discover the power of YAML (Yet Another Markup Language) in network engineering a crucial tool for configuring and managing complex network infrastructures. This article breaks down YAML’s essential components and demonstrates its practical applications, for modern network automation and configuration tasks."
ShowToc: true
cover:
  image: posts/understandingyaml/images/yaml.png
tags:
  - ansible
  - YAML
  - automation
draft: false
---

# Understanding YAML Files: A Guide for Network Engineers

In network engineering, configuration files play a huge role in managing complex network infrastructures and automation. YAML (Yet Another Markup Language) has emerged as a popular format for configuration files due to its simplicity and readability. This article breaks down the components of YAML files and illustrates their significance for network engineers.

## What is YAML?

YAML is a human-readable data serialization standard that can be used to structure data for easy reading and writing. Unlike XML or JSON, YAML's syntax is designed to be easy on the eyes, making it a preferred choice for configuration files and data exchange.

## Core Components of YAML

### 1. Basic Struture

YAML structures data hierarchically using indentation. Each level of indentation represents a deeper level in the hierarchy, The same as folders or directories and subdirectories in a file system. YAML files typically use spaces for indentation (two spaces is common, but it can be more).

**Example**:

```yaml
network:
  interfaces:
    eth0:
      ip: 192.168.1.10
      subnet: 255.255.255.0
```
---

### 2. Key-Value Pairs

YAML relies on key-value pairs to define configuration settings. Keys are always followed by a colon and a space, and values can be scalars (strings, integers, etc.) or nested data structures.

**Example**:

```yaml
hostname: my-router
```
---

### 3. Nested Structures

For more complex configurations, YAML allows for nested key-value pairs using indentation. This is particularly useful for defining hierarchical data such as network settings or device parameters.

**Example**:

``` yaml
firewall:
  rules:
    - port: 22
      action: allow
    - port: 80
      action: deny
```
---

### 4. Lists

YAML supports lists (arrays) that can store multiple items. Lists are indicated by a dash and a space (`- `) preceding each item. Lists can also be nested within other lists or dictionaries.

**Example**:

```yaml
servers:
  - name: server1
    ip: 192.168.1.101
  - name: server2
    ip: 192.168.1.102
```

### 5. Scalars

Scalars are individual, non-divisible values such as strings, integers, or booleans. They can be assigned directly to keys or appear within lists.

**Example**:

```yaml
admin_user: admin
```

---

### 6.  Anchors and Aliases

YAML provides anchors (`&`) and aliases (`*`) to avoid duplication by allowing references to previously defined values. This can be useful for complex configurations where certain values need to be reused.

**Example**:

```yaml
defaults: &defaults
  timeout: 30
  retries: 5

service1:
  <<: *defaults
  url: http://service1.local

service2:
  <<: *defaults
  url: http://service2.local
```

## YAML in Network Engineering

For network engineers, YAML's readability and hierarchical structure make it ideal for managing configurations for networking devices, cloud environments, and automation scripts. YAML can be used to define network topology, device configurations, firewall rules, and more.

### Example Use Case: Network Device Configuration

Imagine you are configuring a series of routers or switches. Instead of manually entering configurations into each device, you can use a YAML file to define the settings centrally and then deploy them automatically.

**Example**:

```yaml
devices:
  - name: router1
    interfaces:
      - name: GigabitEthernet0
        ip: 192.168.1.1
        subnet: 255.255.255.0
      - name: GigabitEthernet1
        ip: 10.0.0.1
        subnet: 255.0.0.0
  - name: switch1
    ports:
      - name: GigabitEthernet1/0/1
        vlan: 10
      - name: GigabitEthernet1/0/2
        vlan: 20
```

In this example, you define each device's interfaces or ports along with their settings. This configuration can be applied across multiple devices, ensuring consistency and reducing manual errors.

**An Example Ansible YAML File**

Ansible uses YAML to define playbooks. Here’s a breakdown of a basic Ansible YAML file for a network engineer:

```yaml
---
- name: Configure network devices
  hosts: switches   # Target group of devices (can be IP addresses or hostnames)

  tasks:
    - name: Ensure VLAN 100 exists
      ios_vlan:  
        vlan_id: 100
        name: Management
        state: present

    - name: Configure interface GigabitEthernet1/0/1
      ios_interface: 
        name: GigabitEthernet1/0/1
        description: Connect to Server A
        state: present
        mode: access
        access_vlan: 100

    - name: Save running configuration
      ios_config:
        save: yes
```

**Lets Break it down**:

1. Name: A descriptive name for your playbook or task.
2. Hosts: Specifies the target devices or groups of devices to configure. In this case, switches could be a group defined in your Ansible inventory file containing the IP addresses or hostnames of your network switches.
3. Tasks: Each task performs a specific configuration action on the network devices. 
 - Ensure VLAN 100 exists:
    - ios_vlan: Ansible module used to manage VLANs on Cisco IOS devices.
    - vlan_id: Specifies the VLAN ID (100 in this case).
    - name: Optional VLAN name (e.g., Management).
    - state: Whether the VLAN should be present

### Advantages of Using YAML
- Readability: YAML's clean and straightforward syntax makes it easy to read and write.
- Simplicity: Minimalistic design with no extraneous characters.
- Hierarchy: Natural support for nested structures aligns well with network configurations.
- Integration: Widely supported in various network automation tools and frameworks like Ansible and SaltStack.

### Conclusion

YAML's structured, human-readable format simplifies the management of network configurations and makes it an great tool for network engineers. By understanding its basic components and capabilities, engineers can streamline their workflows, automate deployments, and maintain robust network infrastructures with ease.