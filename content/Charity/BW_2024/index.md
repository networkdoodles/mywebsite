---
title: "Team Mac & Cheese CHOP Buddy Walk 2024"
date: "2024-06-23"
weight: 1
description: "For those who may not be aware, my son Macario was born 2 years ago with a diagnosis of Trisomy 21, commonly known as Down syndrome. Since receiving this diagnosis, we've been exploring ways to uplift this incredible community. One of the most impactful actions you can take is to support our team, 'Mac & Cheese', at the Buddy Walk"
cover:
  image: Charity/images/BW_2024.png
tags:
  - Charity
  - downsyndrome
  - inclusion
  - CHOP
  - Childrenshospitalofphiladelphia
  - BuddyWalk
draft: false
---

For those who may not be aware, my son Macario was born 2 years ago with a diagnosis of Trisomy 21, commonly known as Down syndrome. Since receiving this diagnosis, we've been exploring ways to uplift this incredible community. One of the most impactful actions you can take is to support our team, 'Mac & Cheese', at the [Buddy Walk]( https://chop.donordrive.com/index.cfm?fuseaction=donorDrive.team&teamID=13920).
 
# Why I Walk:
In the fall of 2021 we were going through many life changes. We were moving our 4 kids to a bigger house, new school district, a new job and we were also had exciting surprise news. When we moved in we found out our precious cargo was most likely positive for Trisomy 21. Needless to say, all our happy life changes became filled with worry as we heard countless professionals give us the worst case scenario.
It wasn't until I spoke with the T21 clinic that I finally received a congratulations on your pregnancy. I was reassured that everything was going to be okay. Things would be different with our new addition but it didn't mean it was the end of the world. I was able to smile and let out a breath of relief.
Our little blessing, Macario (which means "blessing," how perfect is that?), came a bit early. He's had some health stuff to deal with, but this kid is a trooper! Every day is filled with therapy sessions (feeding, talking, all that good stuff) and watching him learn and grow is incredible.
We don't know exactly what the future holds, but one thing's for sure: Macario's gonna rock it, whatever it throws his way.He's got the fighting spirit, the learning bug, and enough cuteness to melt your heart. Here's to this amazing little dude and all the adventures to come!
The CHOP Buddy Walk® provides hope for tomorrow for children with trisomy 21, also known as Down syndrome, the most frequently occurring genetic syndrome in the United States. One in every 800 children are born with Down syndrome, and they often face an uncertain future. These children are at greater risk of developing chronic and life-threatening conditions such as heart disease, bone deformities, neurological, gastrointestinal and endocrine disorders, as well as feeding and developmental disabilities.
 
## How Can You Help?
 
### By donating you’re helping me support:
- Ongoing clinical care and coordination for children and adults with Down syndrome and their families
- New assessment tools to identify potential issues and intervene early
- Continuing education for families, medical providers and community members
- Transformational research to improve the quality — and quantity — of life for individuals with Down syndrome
- With your support, babies born with Down syndrome can experience the many joys of childhood — and flourish in adulthood.

Whether you're in Philly or far away, there's a spot just for you! So, put on your walking shoes, or simply click to support. For my son Macario, for every child, and for a future filled with understanding, love, and boundless possibilities.

**[Join My Team](https://chop.donordrive.com/index.cfm?fuseaction=register.start&eventID=793&teamID=13920)**

**[Donate to my Team](https://chop.donordrive.com/index.cfm?fuseaction=donordrive.team&teamID=13920)**

**[Donate through Facebook](https://www.facebook.com/donate/1916711385419579/)**

**[Event Details](https://chop.donordrive.com/index.cfm?fuseaction=donordrive.event&eventID=793zz)**

**[Get a team T-Shirt](https://www.bonfire.com/macandcheese/?utm_source=copy_link&utm_medium=campaign_page_share&utm_campaign=macandcheese&utm_content=default)**
![t-shirt](images/T-Shirt.png)

**Event Schedule**
- October 20th, 2024
- 9:30 a.m.: Registration begins
- 10:30 a.m.: The grand event kickoff
- 11:30 a.m.: Ceremonial lap commencement
- 1 p.m.: A joyful conclusion to a memorable day
 
**Location**

Lincoln Financial Field
1 Lincoln Financial Field Way
Philadelphia, PA 19148
[Get directions](https://www.google.com/maps/dir/Lincoln+Financial+Field,+One+Lincoln+Financial+Field+Way,+Philadelphia,+PA+19148/@39.9013695,-75.1697155,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x89c6c7a9e03add31:0xf4050036d4293709!2m2!1d-75.1675215!2d39.9013695!3e0)
 
**Transport & Parking**

Easily reachable by personal vehicle or public transport. Park at Lot K (North Entrance).
 
**Registration Fees**:

Preregistration:
- Adults (13+): $15
- Children (4-12): $10
- Under 4s: Free
- Virtual Participant: $15
 
**Event Day Registration**:

- Adults: $20
- Children (4-12): $10
- Under 4s: Still Free!
- Virtual Participant: $15
 
See you [there](https://buddywalk.chop.edu/) – in spirit or stride!
 
### P.S. Don’t forget to share this with friends and family. The more, the merrier!
