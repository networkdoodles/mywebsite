---
title: "Ask me about my tattoo"
date: "2024-07-22"
weight: 2
description: "I got this one for my son. Marcario after we received his diagnosis of trisomy 21(Down syndrome) it’s name comes from the fact that Down syndrome is caused by the child getting a third copy of the 21st chromosome(trisomy 21)."
cover:
  image: Charity/images/tattoo.jpeg
tags:
  - Charity
  - downsyndrome
  - inclusion
  - CHOP
  - Childrenshospitalofphiladelphia
  - BuddyWalk
draft: false
---

I got this one for my son. **Marcario** after we received his diagnosis of trisomy 21 (Down syndrome) it’s name comes from the fact that Down syndrome is caused by the child getting a third copy of the 21st chromosome (trisomy 21).

## Explanation:

An **arrow** must be pulled back in order to shoot forward. This is something many of us have felt when hit by an unexpected diagnosis for our child. This action is preparing us to shoot forward to an amazing future with our incredible kids. 

The Roman numerals for 21 **(XXI)** are included to represent the 21st chromosome and the dots at the end represent trisomy. 

The **rings** in the centre represent the connections made between families in our community, while the overlapping triangles represent strength and 
Resilience.

The inscription the Latin phrase **Omne Trium Perfectum**: everything that is three is perfect.

This is another reference trisomy.

# Support our team, 'Mac & Cheese', at the [Buddy Walk]( https://chop.donordrive.com/index.cfm?fuseaction=donorDrive.team&teamID=13920).