---
title: "Team Mac & Cheese CHOP Buddy Walk 2023"
date: "2024-06-23"
description: "For those who may not be aware, my son Macario was born 2 years ago with a diagnosis of Trisomy 21, commonly known as Down syndrome. Since receiving this diagnosis, we've been exploring ways to uplift this incredible community. One of the most impactful actions you can take is to support our team, 'Mac & Cheese', at the [Buddy Walk]( https://chop.donordrive.com/index.cfm?fuseaction=donorDrive.team&teamID=13920)."
cover:
  image: images/BW_2023.jpeg
tags:
  - Charity
  - downsyndrome
  - inclusion
  - CHOP
  - Childrenshospitalofphiladelphia
  - BuddyWalk
draft: false
---